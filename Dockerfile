# The latest is 7u221. An exact build can be specified
FROM azul/zulu-openjdk:7u352
ARG JAVA_OPTS="-Xms2000M -Xmx2800M"
ENV JAVA_OPTS=$JAVA_OPTS
ENV J2SDKDIR=/usr/lib/jvm/zulu7
ENV J2REDIR=/usr/lib/jvm/zulu7/jre
ENV JAVA_HOME=/usr/lib/jvm/zulu7
ENV PATH=$JAVA_HOME/bin:$JAVA_HOME/db/bin:$JAVA_HOME/jre/bin:$PATH

ENV TZ="Europe/Rome"

# ENV DERBY_HOME=/docker-java-home/db
ENV USER=gcube
ENV GLOBUS_LOCATION=/home/gcube/gCore
ENV ANT_HOME=$GLOBUS_LOCATION
ENV PATH=$GLOBUS_LOCATION/bin:$PATH
#ENV EXIST_HOME=
#ENV GLOBUS_OPTIONS="-Dexist.home=$EXIST_HOME"
ENV GLOBUS_OPTIONS=""
ENV LOGNAME=$USER
ENV GCORE_START_OPTIONS="-DX509_USER_PROXY=$X509_USER_PROXY -Xms2800M -Xmx4800M"

RUN addgroup --system --gid 333 gcube
RUN adduser --system --gecos "Gcube service user" --disabled-password --disabled-login --uid 333 --ingroup gcube --shell /usr/sbin/nologin gcube 
RUN apt-get update
RUN apt-get install -y wget nano less

WORKDIR /home/gcube

RUN cd /home/gcube && wget https://nexus.d4science.org/nexus/content/repositories/gcube-releases/org/gcube/distribution/ghn-distribution/7.0.0-4.2.0-132545/ghn-distribution-7.0.0-4.2.0-132545.tar.gz && tar zxf ghn-distribution-7.0.0-4.2.0-132545.tar.gz && rm -f ghn-distribution-7.0.0-4.2.0-132545.tar.gz && mkdir -p /home/gcube/gCore/logs /home/gcube/gCore/tmp /home/gcube/gCore/config /home/gcube/gCore/etc && chown -R gcube:gcube /home/gcube/gCore
#chown gcube:gcube /home/gcube/gCore/logs /home/gcube/gCore/tmp /home/gcube/gCore/config /home/gcube/gCore/etc 
RUN chmod -R u=rwX,g=rX,o-rwx /home/gcube

RUN rm /home/gcube/gCore/lib/accounting-lib-2.3.0-4.2.0-132276.jar 
RUN rm /home/gcube/gCore/lib/document-store-lib-1.4.0-4.2.0-135110.jar
RUN rm /home/gcube/gCore/lib/document-store-lib-couchbase-1.2.0-4.2.0-131889.jar
# RUN cd /home/gcube && wget https://nexus.d4science.org/nexus/content/repositories/gcube-staging-gcore/org/gcube/distribution/ghn-distribution/7.0.1-4.16.0-144317/ghn-distribution-7.0.1-4.16.0-144317.tar.gz && tar zxf ghn-distribution-7.0.1-4.16.0-144317.tar.gz && rm -f ghn-distribution-7.0.1-4.16.0-144317.tar.gz && mkdir -p /home/gcube/gCore/logs /home/gcube/gCore/tmp /home/gcube/gCore/config /home/gcube/gCore/etc && chown gcube /home/gcube/gCore/logs /home/gcube/gCore/tmp /home/gcube/gCore/config /home/gcube/gCore/etc
# add fake-accounting-lib

# RUN rm /home/gcube/gCore/lib/accounting-lib-3.5.0.jar
# RUN rm /home/gcube/gCore/lib/document-store-lib-2.5.0.jar
# RUN rm /home/gcube/gCore/lib/document-store-lib-couchbase-1.6.0-4.16.0-171307.jar
RUN wget -P /home/gcube/gCore/lib https://nexus.d4science.org/nexus/content/repositories/gcube-snapshots/org/gcube/accounting/fake-accounting-lib/3.2.0-SNAPSHOT/fake-accounting-lib-3.2.0-20190311.105710-2.jar

RUN rm /home/gcube/gCore/lib/is-publisher-2.2.0-4.2.0-126946.jar
RUN wget -P /home/gcube/gCore/lib https://nexus.d4science.org/nexus/content/repositories/gcube-releases-gcore/org/gcube/informationsystem/is-publisher/2.2.1-4.15.0-181697/is-publisher-2.2.1-4.15.0-181697.jar

COPY src/gcube-start-container.sh /home/gcube/gCore/bin/gcore-start-container
COPY src/gcore-start-container-daemon /home/gcube/gCore/bin/gcore-start-container-daemon
COPY src/GHNConfig.xml /home/gcube/gCore/config/GHNConfig.xml
RUN rm /home/gcube/gCore/lib/common-authorization-2.0.1-4.2.0-139995.jar
COPY src/common-authorization-2.0.2-4.3.0.jar /home/gcube/gCore/lib/
RUN rm /home/gcube/gCore/lib/common-scope-maps-1.0.5-4.2.0-134574.jar
COPY src/common-scope-maps-1.1.0-4.15.0-178670.jar /home/gcube/gCore/lib/
COPY src/ServiceMaps.tar.gz /home/gcube/

RUN tar xzvf /home/gcube/ServiceMaps.tar.gz -C /home/gcube/gCore/config/
RUN chmod 750 /home/gcube/gCore/config/ServiceMap_*.xml

# Update loggin settings
# Log with debug
# COPY src/container-log4j.properties.stdout.debug /home/gcube/gCore/container-log4j.properties
COPY src/container-log4j.properties /home/gcube/gCore/
COPY src/logging.jul.properties /home/gcube/gCore/
RUN chown -R gcube:gcube /home/gcube/gCore

# COPY src/log4j-stdout-appender.patch /home/gcube/gCore/
# RUN cat /home/gcube/gCore/log4j-stdout-appender.patch >> /home/gcube/gCore/container-log4j.properties
# RUN sed -i 's/log4j.rootLogger=ERROR,FULL,LITE/log4j.rootLogger=ERROR,stdout/' /home/gcube/gCore/container-log4j.properties
RUN $GLOBUS_LOCATION/bin/gcore-load-env
RUN chmod 755 /home/gcube/gCore/bin/gcore-start-container
# Remove Resultset from distribution
# RUN /home/gcube/gCore/bin/gcore-undeploy-service org.gcube.common.searchservice.resultsetservice
# RUN /home/gcube/gCore/bin/gcore-undeploy-service resultsetservice-3.1.0-4.16.0-126939
RUN /home/gcube/gCore/bin/gcore-undeploy-service resultsetservice-3.1.0-4.2.0-126939
RUN mkdir /gcube-data && chown gcube.nogroup /gcube-data


USER gcube
EXPOSE 8080
CMD /home/gcube/gCore/bin/gcore-start-container
# ENTRYPOINT exec /home/gcube/gCore/bin/gcore-start-container
# ENTRYPOINT ["tail", "-f", "/dev/null"]
# For Spring-Boot project, use the entrypoint below to reduce Tomcat startup time.
#ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar ansibleplaybookisregistryservice.jar
