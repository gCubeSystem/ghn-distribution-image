# gCore Hosting Node (gHN) distribution Docker image

The included Docker file will build a default image for the gHN distribution

## Build the image

```shell
$ docker build -t gcore-distribution .
```

## Run the image

```shell
$ docker container run --name gcore-hosting-node -p 8080:8080 gcore-distribution 
```
