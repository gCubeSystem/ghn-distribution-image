#! /bin/bash


# if [ -n "$EXIST_HOME" ]; then
#   # our container is running a IS Collector
#   if [ -L "$EXIST_HOME/webapp/WEB-INF/data" ] ; then
#       # exist data has been already mounted on NFS
#       echo "eXist data should be already in place on NFS"
#   elif [ -d "$EXIST_HOME/webapp/WEB-INF/data" ] ; then
#       echo "First run: set up our directory structure on the NFS mountpoint"
#       mkdir -p /gcube-data/{db,backup} && mv $EXIST_HOME/webapp/WEB-INF/{data,logs,attachments} /gcube-data/db/
#       if [ $? -eq 0 ]; then
#         for dir in {data,logs,attachments}; do
#           ln -s /gcube-data/db/${dir} $EXIST_HOME/webapp/WEB-INF/${dir}
#         done
#       else
#         echo "Move failed: data already exists on NFS"
#         exit 1
#       fi
#   else
#       echo "Not a link, not a directory: a pony :-)?"
#   fi
# fi

# mkdir -p /gcube-data/db/{data,logs,attachments}
# mkdir -p $NFS_GCORE_PATH/persisted
# mkdir $HOME/.gcore && chown gcube:gcube $HOME/.gcore
# ln -s $NFS_GCORE_PATH/persisted $HOME/.gcore/


if [ -z "$GLOBUS_LOCATION"  ]
then
	echo  "Cannot start the GHN, GLOBUS_LOCATION var is not set!"
	exit 1
fi

if [ ! -d "$GLOBUS_LOCATION/config"  ]
then
	echo  "Cannot start the GHN, GLOBUS_LOCATION var is wrongly set!"
	exit 1
fi

CP=.:${GLOBUS_LOCATION}:${GLOBUS_LOCATION}/config:${GLOBUS_LOCATION}/build/classes
for i in ${GLOBUS_LOCATION}/lib/*.jar
do
   CP=$CP:"$i"
done

if [ -z "$CLASSPATH" ] ; then
  CLASSPATH=$CP
else
  CLASSPATH=$CP:$CLASSPATH
fi
export CLASSPATH

#container's options
export GCORE_START_OPTIONS="$GCORE_START_OPTIONS -Djava.util.logging.config.file=$GLOBUS_LOCATION/logging.jul.properties $JAVA_OPTS -Djava.io.tmpdir=$GLOBUS_LOCATION/tmp  -Djava.security.egd=file:///dev/urandom"

mode="-nosec"
unset newargs

for arg in "$@"
do
    if [ "$arg" = "-sec" ]; then
        unset mode
    else
        newargs="$newargs $arg"
    fi
done

newargs="$mode $newargs"

[ -e "$GLOBUS_LOCATION/tmp" ] ||  mkdir -p $GLOBUS_LOCATION/tmp

#clean up the services' state if needed
#[ -f "$GLOBUS_LOCATION/config/GHNProfile.xml" ] ||  rm -rf ~/.gcore

cd $GLOBUS_LOCATION
echo "gCore is running with ($newargs) and logging into logs/container.log..."
$GLOBUS_LOCATION/bin/gcore-start-container-daemon $newargs
